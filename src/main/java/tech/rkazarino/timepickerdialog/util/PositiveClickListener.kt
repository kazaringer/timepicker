package tech.rkazarino.timepickerdialog.util

interface PositiveClickListener {
    fun onClick(time: String)
}