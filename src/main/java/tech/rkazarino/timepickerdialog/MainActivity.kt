package tech.rkazarino.timepickerdialog

import android.app.DatePickerDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import tech.rkazarino.timepickerdialog.util.CustomTimePickerDialog
import tech.rkazarino.timepickerdialog.util.PositiveClickListener
import java.util.*

class MainActivity : AppCompatActivity() {
    private val timePickerDialog = CustomTimePickerDialog()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setListeners()
    }

    private fun setListeners() {
        timePickerDialog.setOnPositiveClickListener(object : PositiveClickListener {
            override fun onClick(time: String) {
                textViewTime.text = time
            }
        })

        buttonShowDialog.setOnClickListener {
            showTimePickerDialog()
        }
    }

    private fun showTimePickerDialog() {
        timePickerDialog.show(supportFragmentManager, null)
    }


}
